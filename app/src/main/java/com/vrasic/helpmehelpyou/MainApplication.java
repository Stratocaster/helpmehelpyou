package com.vrasic.helpmehelpyou;

import android.app.Application;

import com.vrasic.helpmehelpyou.service.AuthManager;
import com.vrasic.helpmehelpyou.service.LogManager;


public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        LogManager.init();
        AuthManager.getInstance().init();
    }

}
