package com.vrasic.helpmehelpyou.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Profile implements Cloneable {

    public String id;
    public String username;
    public WebRTCAccessToken webRTCAccessToken;

    public Profile() {}

    public Profile(String username, WebRTCAccessToken webRTCAccessToken) {
        this.username = username;
        this.webRTCAccessToken = webRTCAccessToken;
    }

    public boolean isValidWebRTCAccessToken() {
        return this.webRTCAccessToken != null;
//        return (webRTCAccessToken != null && webRTCAccessToken.identity != null && webRTCAccessToken.expirationTime != null &&
//                webRTCAccessToken.identity.equals(id) &&
//                new Date(webRTCAccessToken.expirationTime).after(new Date()));
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("username", this.username);
        map.put("webRTCAccessToken", this.webRTCAccessToken);

        return map;
    }

    public String toString() {
        return
            "{ "
                + "username: " + this.username
                + "webRTCAccessToken: " + this.webRTCAccessToken
            + " }";
    }

    public Profile makeClone() {
        try {
            Profile profile = (Profile) this.clone();
            if (this.webRTCAccessToken != null) {
                profile.webRTCAccessToken = this.webRTCAccessToken.makeClone();
            }
            return profile;
        }
        catch (Exception e) {
            return null;
        }
    }
}
