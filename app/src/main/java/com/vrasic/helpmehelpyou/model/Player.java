package com.vrasic.helpmehelpyou.model;

import java.util.HashMap;
import java.util.Map;

public class Player implements Cloneable {

    public String id;
    public String username;
    public Boolean isHost;
    public Boolean isReady;

    public Player() {}

    public Player(String username, Boolean isHost, Boolean isReady) {
        this.username = username;
        this.isHost = isHost;
        this.isReady = isReady;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("username", this.username);
        map.put("isHost", this.isHost);
        map.put("isReady", this.isReady);

        return map;
    }

    public Player makeClone() {
        try {
            return (Player) this.clone();
        }
        catch (Exception e) {
            return null;
        }
    }
}
