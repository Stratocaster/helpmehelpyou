package com.vrasic.helpmehelpyou.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WebRTCAccessToken implements Cloneable {

    public String accessToken;
    public String identity;
    public Long expirationTime;

    public WebRTCAccessToken() {}

    public WebRTCAccessToken(String accessToken, String identity, Date expirationTime) {
        this(accessToken, identity, expirationTime != null ? expirationTime.getTime() : null);
    }

    public WebRTCAccessToken(String accessToken, String identity, Long expirationTime) {
        this.accessToken = accessToken;
        this.identity = identity;
        this.expirationTime = expirationTime;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("token", this.accessToken);
        map.put("identity", this.identity);
        map.put("expirationTime", this.expirationTime);

        return map;
    }

    @Override
    public String toString() {
        return "TokenResponse{" +
                "token='" + accessToken + '\'' +
                ", identity='" + identity + '\'' +
                ", expirationTime=" + expirationTime +
                '}';
    }

    public WebRTCAccessToken makeClone() {
        try {
            return (WebRTCAccessToken) this.clone();
        }
        catch (Exception e) {
            return null;
        }
    }
}
