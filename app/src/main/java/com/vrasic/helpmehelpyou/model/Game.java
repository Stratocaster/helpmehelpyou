package com.vrasic.helpmehelpyou.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Game implements Cloneable {

    public enum Status {
        WAITING_OPPONENT("waiting_opponent"),
        WAITING_APPROVAL("waiting_approval"),
        ACTIVE("active"),
        FINISHED("finished");

        private String value;
        Status(String value) {
            this.value = value;
        }
        public String getValue() {
            return this.value;
        }
    }

    public static final String DEFAULT_STATUS = Game.Status.WAITING_OPPONENT.getValue();
    public static final Integer DEFAULT_TIMER = 300;
    public static final Integer DEFAULT_MISS = 0;

    public String id;
    public HashMap<String, Player> players;
    public String lastPlayerRequestingApproval;
    public String status;
    public Integer timer;
    public Integer miss;

    public Game() {}

    public Game(HashMap<String, Player> players, String status, Integer timer, Integer miss) {
        this.players = players;
        this.status  = status != null ? status : DEFAULT_STATUS;
        this.timer   = timer != null ? timer : DEFAULT_TIMER;
        this.miss    = miss != null ? miss : DEFAULT_MISS;
        this.lastPlayerRequestingApproval = null;
    }

    public boolean isWaitingOpponent() {
        return this.status.equals(Game.Status.WAITING_OPPONENT.getValue());
    }

    public boolean isWaitingApproval() {
        return this.status.equals(Game.Status.WAITING_APPROVAL.getValue());
    }

    public boolean isActive() {
        return this.status.equals(Game.Status.ACTIVE.getValue());
    }

    public boolean isFinished() {
        return this.status.equals(Game.Status.FINISHED.getValue());
    }

    public void resetToDefaults() {
        this.status = DEFAULT_STATUS;
        this.timer  = DEFAULT_TIMER;
        this.miss   = DEFAULT_MISS;

        if (this.players.size() != 1) {
            return;
        }

        Map.Entry<String, Player> entry = this.players.entrySet().iterator().next();
        String key = entry.getKey();
        Player player = entry.getValue();

        player.isReady = false;
        player.isHost = true;

        HashMap<String, Player> newPlayers = new HashMap<>();
        newPlayers.put(key, player);

        this.players = newPlayers;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("players", this.players);
        map.put("status", this.status);
        map.put("timer", this.timer);
        map.put("miss", this.miss);
        map.put("lastPlayerRequestingApproval", this.lastPlayerRequestingApproval);

        return map;
    }

    public Game makeClone() {
        try {
            Game clonedGame = (Game) this.clone();
            HashMap<String, Player> clonedPlayers = new HashMap<>();

            for (String key : this.players.keySet()) {
                clonedPlayers.put(key, this.players.get(key).makeClone());
            }

            clonedGame.players = clonedPlayers;
            return clonedGame;
        }
        catch (Exception e) {
            return null;
        }
    }
}
