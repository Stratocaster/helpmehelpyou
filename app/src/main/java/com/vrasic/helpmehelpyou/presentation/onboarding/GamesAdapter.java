package com.vrasic.helpmehelpyou.presentation.onboarding;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.RecyclerView;

import com.vrasic.helpmehelpyou.R;
import com.vrasic.helpmehelpyou.data.GamesRepository;
import com.vrasic.helpmehelpyou.data.ProfileRepository;
import com.vrasic.helpmehelpyou.event.GameJoined;
import com.vrasic.helpmehelpyou.model.Game;
import com.vrasic.helpmehelpyou.model.Player;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView playersTextView;
        public Button actionButton;

        public ViewHolder(View itemView) {
            super(itemView);

            playersTextView = itemView.findViewById(R.id.players_title);
            actionButton = itemView.findViewById(R.id.action_button);
        }
    }

    private CopyOnWriteArrayList<Game> games;
    private Context context;

    public GamesAdapter(CopyOnWriteArrayList<Game> games) {
        this.games = games;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(this.context);

        View gamesListItemView = inflater.inflate(R.layout.games_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(gamesListItemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Game game = this.games.get(position);

        ArrayList<String> playerNames = new ArrayList<>();
        for (Player player : game.players.values()) {
            playerNames.add(player.username);
        }

        holder.playersTextView.setText(TextUtils.join(" - ", playerNames));

        holder.actionButton.setText(game.isWaitingOpponent() ? "Join" : game.isFinished() ? "Finished" : "Ongoing");
        holder.actionButton.setEnabled(game.isWaitingOpponent());

        holder.actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GamesRepository.getInstance().joinGame(game.id, ProfileRepository.getInstance().getProfile(), new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean success) {
                        if (!success) {
                            Toast.makeText(GamesAdapter.this.context,"Could not join game at this moment!",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        EventBus.getDefault().post(new GameJoined(game.id));
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.games.size();
    }
}
