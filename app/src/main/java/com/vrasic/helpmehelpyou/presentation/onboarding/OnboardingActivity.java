package com.vrasic.helpmehelpyou.presentation.onboarding;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.core.util.Consumer;
import androidx.fragment.app.FragmentActivity;

import com.vrasic.helpmehelpyou.data.ProfileRepository;
import com.vrasic.helpmehelpyou.data.WebRTCRepository;
import com.vrasic.helpmehelpyou.event.ProfileLoaded;
import com.vrasic.helpmehelpyou.event.UserSignedIn;
import com.vrasic.helpmehelpyou.event.UserSignedOut;
import com.vrasic.helpmehelpyou.model.Profile;
import com.vrasic.helpmehelpyou.model.WebRTCAccessToken;
import com.vrasic.helpmehelpyou.service.AuthManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import timber.log.Timber;

public class OnboardingActivity extends FragmentActivity {

    private FrameLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        EventBus.getDefault().register(this);

        this.mainLayout = new FrameLayout(this);
        this.mainLayout.setId(View.generateViewId());
        FrameLayout.LayoutParams mainLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        setContentView(this.mainLayout, mainLayoutParams);

        this.showLoaderFragment();
    }

    private void showLoaderFragment() {
        if (this.getSupportFragmentManager().findFragmentByTag(LoaderFragment.FRAGMENT_TAG) != null) {
            return;
        }

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(this.mainLayout.getId(), new LoaderFragment(), LoaderFragment.FRAGMENT_TAG)
                .commit();
    }

    private void showSignInFragment() {
        if (this.getSupportFragmentManager().findFragmentByTag(SignInFragment.FRAGMENT_TAG) != null) {
            return;
        }

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(this.mainLayout.getId(), new SignInFragment(), SignInFragment.FRAGMENT_TAG)
                .commit();
    }

    private void showGamesListFragment() {
        if (this.getSupportFragmentManager().findFragmentByTag(GamesListFragment.FRAGMENT_TAG) != null) {
            return;
        }

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(this.mainLayout.getId(), new GamesListFragment(), GamesListFragment.FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        String userId = AuthManager.getInstance().getCurrentUserUid();

        if (userId == null) {
            AuthManager.getInstance().signIn();
        } else {
            ProfileRepository.getInstance().startListeningForProfileChanges(userId);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

        ProfileRepository.getInstance().stopListeningForProfileChanges();
    }

    @Subscribe
    public void onProfileLoaded(ProfileLoaded event) {
        final Profile profile = event.getProfile();
        Timber.i("Profile loaded: %s", profile);

        if (profile == null) {
            this.showSignInFragment();
        } else {
            if (!profile.isValidWebRTCAccessToken()) {
                WebRTCRepository.getInstance().getAccessToken(profile.id.substring(0, 20), webRTCAccessToken -> {
                    profile.webRTCAccessToken = webRTCAccessToken;
                    ProfileRepository.getInstance().saveProfile(profile);
                });
            }
            this.showGamesListFragment();
        }
    }

    @Subscribe
    public void onUserSignedIn(UserSignedIn event) {
        Timber.i("User signed in with UID: %s", event.getUserUid());
        ProfileRepository.getInstance().startListeningForProfileChanges(event.getUserUid());
    }


    @Subscribe
    public void onUserSignedOut(UserSignedOut event) {
        Timber.i("User signed out!");
    }


}
