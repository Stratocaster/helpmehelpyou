package com.vrasic.helpmehelpyou.presentation.play;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.vrasic.helpmehelpyou.R;
import com.vrasic.helpmehelpyou.data.GameRepository;
import com.vrasic.helpmehelpyou.data.GamesRepository;
import com.vrasic.helpmehelpyou.data.ProfileRepository;
import com.vrasic.helpmehelpyou.event.GameLoaded;
import com.vrasic.helpmehelpyou.model.Game;
import com.vrasic.helpmehelpyou.model.Player;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Collection;
import java.util.List;

import timber.log.Timber;

public class PlayActivity extends FragmentActivity {

    public static final String GAME_ID_EXTRA = "GAME_ID_EXTRA";
    public static final String REQUEST_APPROVAL_EXTRA = "REQUEST_APPROVAL_EXTRA";
    private FrameLayout fragmentContainer;
    private ViewGroup topHudView;
    private TextView timerTextView;
    private String gameId;
    private boolean shouldRequestApproval;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        EventBus.getDefault().register(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.gameId = extras.getString(PlayActivity.GAME_ID_EXTRA);
            this.shouldRequestApproval = extras.getBoolean(PlayActivity.REQUEST_APPROVAL_EXTRA);
            if (this.gameId == null) {
                Timber.d("No gameId passed to PlayActivity");
                this.finish();
            }
        }

        setContentView(R.layout.play_activity);

        this.fragmentContainer = findViewById(R.id.frame_container);
        this.topHudView = findViewById(R.id.top_hud);
        this.timerTextView = this.topHudView.findViewById(R.id.timer);

        this.showHandshakeFragment();
    }

    private void showHandshakeFragment() {
        if (this.getSupportFragmentManager().findFragmentByTag(HandshakeFragment.FRAGMENT_TAG) != null) {
            return;
        }

        this.topHudView.setVisibility(View.GONE);

        HandshakeFragment handshakeFragment = new HandshakeFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("shouldRequestApproval", this.shouldRequestApproval);
        handshakeFragment.setArguments(bundle);

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(this.fragmentContainer.getId(), handshakeFragment, HandshakeFragment.FRAGMENT_TAG)
                .commit();
    }

    private void showWarmupFragment() {
        if (this.getSupportFragmentManager().findFragmentByTag(WarmupFragment.FRAGMENT_TAG) != null) {
            return;
        }

        this.topHudView.setVisibility(View.VISIBLE);

        WarmupFragment warmupFragment = new WarmupFragment();
        Bundle bundle = new Bundle();
        bundle.putString("gameId", this.gameId);
        warmupFragment.setArguments(bundle);

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(this.fragmentContainer.getId(), warmupFragment, WarmupFragment.FRAGMENT_TAG)
                .commit();
    }

    private void showHostPlayFragment() {
        if (this.getSupportFragmentManager().findFragmentByTag(HostPlayFragment.FRAGMENT_TAG) != null) {
            return;
        }

        HostPlayFragment hostPlayFragment = new HostPlayFragment();

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(this.fragmentContainer.getId(), hostPlayFragment, HostPlayFragment.FRAGMENT_TAG)
                .commit();
    }

    private void showGuestPlayFragment() {
        if (this.getSupportFragmentManager().findFragmentByTag(GuestPlayFragment.FRAGMENT_TAG) != null) {
            return;
        }

        GuestPlayFragment guestPlayFragment = new GuestPlayFragment();

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(this.fragmentContainer.getId(), guestPlayFragment, GuestPlayFragment.FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        GameRepository.getInstance().startListeningForGameChanges(this.gameId);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

        GameRepository.getInstance().stopListeningForGameChanges();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (this.countDownTimer != null) {
            this.countDownTimer.cancel();
        }
    }

    @Subscribe
    public void onGameLoaded(GameLoaded event) {
        Game game = event.getGame();

        if (game == null) {
            this.closeActivity("Game deleted on server-side!", "Game deleted!");
            return;
        }

        if (!game.players.containsKey(ProfileRepository.getInstance().getProfile().id)) {
            this.closeActivity("Player removed from game on the server-side!", "Game canceled!");
            return;
        }

        if (game.players.size() == 1 && game.lastPlayerRequestingApproval != null && game.lastPlayerRequestingApproval.equals(ProfileRepository.getInstance().getProfile().id)) {
            GamesRepository.getInstance().leaveGame(game.id, ProfileRepository.getInstance().getProfile().id, null);
            return;
        }

        if (game.players.size() == 2 && this.getSupportFragmentManager().findFragmentByTag(WarmupFragment.FRAGMENT_TAG) != null) {
            Collection<Player> players = game.players.values();

            boolean playersReady = true;

            for (Player player : players) {
                playersReady = playersReady && player.isReady;
            }

            if (playersReady) {
                Player me = game.players.get(ProfileRepository.getInstance().getProfile().id);
                if (me != null) {
                    if (me.isHost) {
                        this.showHostPlayFragment();
                    }
                    else {
                        this.showGuestPlayFragment();
                    }

                    this.startCountDownTimer(game);
                }
            }
        }

        if (game.status.equals(Game.Status.ACTIVE.getValue())) {
            this.showWarmupFragment();
        }

        if (game.status.equals(Game.Status.WAITING_OPPONENT.getValue())) {
            this.showHandshakeFragment();
            //this.showWarmupFragment();
            //this.showHostPlayFragment();
            //this.showGuestPlayFragment();
        }


    }

    private void startCountDownTimer(Game game) {
        this.countDownTimer = new CountDownTimer(game.timer * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                long minutes = millisUntilFinished / (60 * 1000);
                long seconds = (millisUntilFinished / 1000) % 60;
                String time = String.format("%d:%02d", minutes, seconds);
                PlayActivity.this.timerTextView.setText(String.format("%s", time));
            }

            public void onFinish() {
                //mTextField.setText("done!");
            }

        }.start();
    }

    public void closeActivity(String logMessage, String toastMessage) {
        Timber.i(logMessage);
        Toast.makeText(this,toastMessage,Toast.LENGTH_SHORT).show();
        this.finish();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Leave game")
                .setMessage("Are you sure you want to leave this game?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GamesRepository.getInstance().leaveGame(PlayActivity.this.gameId, ProfileRepository.getInstance().getProfile().id, null);
                        PlayActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }
}
