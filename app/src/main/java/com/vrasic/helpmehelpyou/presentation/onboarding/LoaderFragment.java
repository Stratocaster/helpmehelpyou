package com.vrasic.helpmehelpyou.presentation.onboarding;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vrasic.helpmehelpyou.R;

public class LoaderFragment extends Fragment {

    public static final String FRAGMENT_TAG = "loader-fragment";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.loader_fragment, container, false);
        return view;
    }
}
