package com.vrasic.helpmehelpyou.presentation.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vrasic.helpmehelpyou.R;
import com.vrasic.helpmehelpyou.data.GamesRepository;
import com.vrasic.helpmehelpyou.data.ProfileRepository;
import com.vrasic.helpmehelpyou.event.GameCreated;
import com.vrasic.helpmehelpyou.event.GameJoined;
import com.vrasic.helpmehelpyou.event.GamesLoaded;
import com.vrasic.helpmehelpyou.model.Game;
import com.vrasic.helpmehelpyou.presentation.play.PlayActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.concurrent.CopyOnWriteArrayList;

import timber.log.Timber;

public class GamesListFragment extends Fragment {

    private GamesAdapter adapter;
    private RecyclerView recyclerView;
    private CopyOnWriteArrayList<Game> games = new CopyOnWriteArrayList<>();

    public static final String FRAGMENT_TAG = "games-fragment";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.games_list_fragment, container, false);

        this.recyclerView = rootView.findViewById(R.id.games_list);
        this.recyclerView.setHasFixedSize(true);

        Button createGameButton = rootView.findViewById(R.id.game_create);
        createGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GamesRepository.getInstance().createGame(ProfileRepository.getInstance().getProfile(), new Consumer<String>() {
                    @Override
                    public void accept(String gameId) {
                        if (gameId == null) {
                            Toast.makeText(container.getContext(),"Could not join game at this moment!",Toast.LENGTH_SHORT).show();
                            return;
                        }

                        EventBus.getDefault().post(new GameCreated(gameId));
                    }
                });
            }
        });

        return rootView;
    }

    private void testPlayActivity() {
        Intent intent = new Intent(getActivity(), PlayActivity.class);
        startActivity(intent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        this.recyclerView.setLayoutManager(linearLayoutManager);

        this.adapter = new GamesAdapter(this.games);
        this.recyclerView.setAdapter(this.adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        //this.refreshGamesList(this.gamesFiltered(GamesRepository.getInstance().getGames()));
        GamesRepository.getInstance().startListeningForGamesChanges();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        GamesRepository.getInstance().stopListeningForGamesChanges();
    }

    @Subscribe
    public void onGamesLoaded(GamesLoaded event) {
        Timber.i("Games loaded: %s", event.getGames().size());

        this.refreshGamesList(event.getGames());
    }

    @Subscribe
    public void onGameCreated(GameCreated event) {
        Timber.i("Game created: %s", event.getGameId());

        Intent intent = new Intent(getActivity(), PlayActivity.class);
        intent.putExtra(PlayActivity.GAME_ID_EXTRA, event.getGameId());
        startActivity(intent);
    }

    @Subscribe
    public void onGameJoined(GameJoined event) {
        Timber.i("Game joined: %s", event.getGameId());

        Intent intent = new Intent(getActivity(), PlayActivity.class);
        intent.putExtra(PlayActivity.GAME_ID_EXTRA, event.getGameId());
        intent.putExtra(PlayActivity.REQUEST_APPROVAL_EXTRA, true);
        startActivity(intent);
    }

    private void refreshGamesList(CopyOnWriteArrayList<Game> games) {
        this.games.clear();
        this.games.addAll(this.gamesFiltered(games));
        this.adapter.notifyDataSetChanged();
    }

    private CopyOnWriteArrayList<Game> gamesFiltered(CopyOnWriteArrayList<Game> games) {
        CopyOnWriteArrayList<Game> gamesFiltered = new CopyOnWriteArrayList<>();

        for (Game game : games) {
            // Filter out games with no players
            if (game.players == null || game.players.values().size() == 0) {
                continue;
            }

            // Filter out games occupied only by me
            if (game.players.values().size() == 1 && game.players.containsKey(ProfileRepository.getInstance().getProfile().id)) {
                continue;
            }

            gamesFiltered.add(game);
        }

        return gamesFiltered;
    }

}
