package com.vrasic.helpmehelpyou.presentation.play;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.infobip.webrtc.sdk.api.call.CallRequest;
import com.vrasic.helpmehelpyou.R;
import com.vrasic.helpmehelpyou.data.GameRepository;
import com.vrasic.helpmehelpyou.data.GamesRepository;
import com.vrasic.helpmehelpyou.data.ProfileRepository;
import com.vrasic.helpmehelpyou.event.CallListener;
import com.vrasic.helpmehelpyou.event.GameLoaded;
import com.vrasic.helpmehelpyou.model.Game;
import com.vrasic.helpmehelpyou.model.Player;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collection;

import timber.log.Timber;

public class WarmupFragment extends Fragment {

    public static final String FRAGMENT_TAG = "warmup-fragment";

    private TextView hostTextView;
    private TextView guestTextView;
    private Button toggleButton;
    private Button readyButton;
    private String gameId;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        EventBus.getDefault().register(this);

        View rootView = inflater.inflate(R.layout.warmup_fragment, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            this.gameId = bundle.getString("gameId");
        }

        this.hostTextView = rootView.findViewById(R.id.host);
        this.guestTextView = rootView.findViewById(R.id.guest);
        this.toggleButton = rootView.findViewById(R.id.toggle);
        this.readyButton = rootView.findViewById(R.id.ready);

        this.toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GamesRepository.getInstance().toggleGameHost(WarmupFragment.this.gameId, null);
            }
        });

        this.readyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GamesRepository.getInstance().togglePlayerReady(WarmupFragment.this.gameId, ProfileRepository.getInstance().getProfile().id, null);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        this.refreshTitle(GameRepository.getInstance().getGame());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        this.refreshTitle(null);
    }

    @Subscribe
    public void onGameLoaded(GameLoaded event) {
        Game game = event.getGame();
        Timber.i("Game loaded: %s", game != null ? game.toMap() : null);

        this.refreshTitle(game);
        establishCall();
    }

    private void establishCall() {
        String accessToken = ProfileRepository.getInstance().getProfile().webRTCAccessToken.accessToken;
        final String myId = ProfileRepository.getInstance().getProfile().id;
        String otherPlayerId = getOtherPlayerId(myId);
        CallRequest callRequest = new CallRequest(accessToken, this.getContext(), otherPlayerId, new CallListener());
    }

    private String getOtherPlayerId(String myId) {
        Collection<Player> players = GameRepository.getInstance().getGame().players.values();
        String otherPlayer = "";
        for (Player p : players) {
            if (p.id != null && !p.id.equals(myId)) {
                otherPlayer = p.id;
            }
        }
        return otherPlayer;
    }


    private void refreshTitle(Game game) {
        ArrayList<String> playerNames = new ArrayList<>();

        Player hostPlayer = null;
        Player guestPlayer = null;

        if (game != null) {
            for (Player player : game.players.values()) {
                playerNames.add(player.username);
                if (player.isHost) {
                    hostPlayer = player;
                } else {
                    guestPlayer = player;
                }
            }
        }

        Timber.d("REFRESHING TITLE: %s", playerNames);

        int greenColor = ContextCompat.getColor(this.getContext(), android.R.color.holo_green_dark);
        int blackColor = ContextCompat.getColor(this.getContext(), android.R.color.black);

        boolean hostIsReady = hostPlayer != null ? hostPlayer.isReady : false;
        boolean guestIsReady = guestPlayer != null ? guestPlayer.isReady : false;

        hostTextView.setText(String.format("Host player: %s", hostPlayer != null ? hostPlayer.username : null));
        hostTextView.setTextColor(hostIsReady ? greenColor : blackColor);
        guestTextView.setText(String.format("Guest player: %s", guestPlayer != null ? guestPlayer.username : null));
        guestTextView.setTextColor(guestIsReady ? greenColor : blackColor);

        if (game != null && game.players != null && game.players.size() == 2) {
            toggleButton.setEnabled(true);

            Player me = game.players.containsKey(ProfileRepository.getInstance().getProfile().id) ? game.players.get(ProfileRepository.getInstance().getProfile().id) : null;
            boolean iAmReady = me != null ? me.isReady : false;
            readyButton.setEnabled(me != null);
            readyButton.setText(iAmReady ? "Not ready" : "Ready");
        } else {
            toggleButton.setEnabled(false);
            readyButton.setEnabled(false);
        }
    }
}
