package com.vrasic.helpmehelpyou.presentation.play;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vrasic.helpmehelpyou.R;

public class HostPlayFragment extends Fragment {

    public static final String FRAGMENT_TAG = "hostplay-fragment";
    private TextView manual1TextView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        //EventBus.getDefault().register(this);

        View rootView = inflater.inflate(R.layout.hostplay_fragment, container, false);

        this.manual1TextView = rootView.findViewById(R.id.manual1);

        this.populateContent();

        return rootView;
    }

    private void populateContent() {
        this.manual1TextView.setText(getResources().getString(R.string.manual_1));
    }
}
