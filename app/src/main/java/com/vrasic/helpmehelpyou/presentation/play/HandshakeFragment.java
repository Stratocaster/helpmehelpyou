package com.vrasic.helpmehelpyou.presentation.play;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;

import com.vrasic.helpmehelpyou.R;
import com.vrasic.helpmehelpyou.data.GameRepository;
import com.vrasic.helpmehelpyou.data.GamesRepository;
import com.vrasic.helpmehelpyou.data.ProfileRepository;
import com.vrasic.helpmehelpyou.event.GameLoaded;
import com.vrasic.helpmehelpyou.model.Game;
import com.vrasic.helpmehelpyou.model.Player;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import timber.log.Timber;

public class HandshakeFragment extends Fragment {

    public static final String FRAGMENT_TAG = "handshake-fragment";

    private TextView titleTextView;
    private boolean shouldRequestApproval;
    private ProgressDialog waitingDialog;
    private AlertDialog approveDialog;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        EventBus.getDefault().register(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            this.shouldRequestApproval = bundle.getBoolean("shouldRequestApproval");
        }

        View rootView = inflater.inflate(R.layout.handshake_fragment, container, false);

        this.titleTextView = rootView.findViewById(R.id.handshake_title);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        this.refreshTitle(GameRepository.getInstance().getGame());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        this.refreshTitle(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (this.waitingDialog != null) {
            this.waitingDialog.dismiss();
        }
    }

    @Subscribe
    public void onGameLoaded(GameLoaded event) {
        final Game game = event.getGame();
        Timber.i("Game loaded: %s", game != null ? game.toMap() : null);

        this.refreshTitle(game);

        if (game != null) {
            if (this.shouldRequestApproval) {
                this.shouldRequestApproval = false;

                GamesRepository.getInstance().requestJoinApproval(game.id, ProfileRepository.getInstance().getProfile().id, new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean success) {
                        if (success) {
                            HandshakeFragment.this.showWaitingDialog(game);
                        }
                    }
                });
            }
            else if (game.status.equals(Game.Status.WAITING_APPROVAL.getValue()) && !game.lastPlayerRequestingApproval.equals(ProfileRepository.getInstance().getProfile().id)) {
                this.showApproveDialog(game);
            }
            else if (game.lastPlayerRequestingApproval != null && !game.players.containsKey(game.lastPlayerRequestingApproval) && this.approveDialog != null && this.approveDialog.isShowing()) {
                this.approveDialog.dismiss();
            }
        }
    }

    private void showWaitingDialog(final Game game) {
        this.waitingDialog = ProgressDialog.show(this.getContext(), "","Waiting approval...", true);
        this.waitingDialog.setCancelable(true);
        this.waitingDialog.setCanceledOnTouchOutside(false);
        this.waitingDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
            @Override
            public void onCancel(DialogInterface dialog){
                GamesRepository.getInstance().leaveGame(game.id, ProfileRepository.getInstance().getProfile().id, null);
                HandshakeFragment.this.getActivity().finish();
            }});
    }

    private void showApproveDialog(final Game game) {
        Player opponent = game.players.get(game.lastPlayerRequestingApproval);
        this.approveDialog = new AlertDialog.Builder(this.getContext())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("JOIN REQUEST")
                .setMessage(String.format("Player %s wants to join the game?", opponent != null ? opponent.username : null))
                .setPositiveButton("Approve", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GamesRepository.getInstance().approveJoinRequest(game.id, null);
                    }
                })
                .setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GamesRepository.getInstance().leaveGame(game.id, game.lastPlayerRequestingApproval, null);
                    }
                })
                .show();
    }

    private void refreshTitle(Game game) {
        String opponentUsername = "";

        if (game != null) {
            Player opponent = game.players.get(game.lastPlayerRequestingApproval);

            if (opponent != null) {
                opponentUsername = opponent.username;
            }
        }

        titleTextView.setText(String.format("Waiting for opponent... \n%s", opponentUsername));
    }
}
