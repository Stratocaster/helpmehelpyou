package com.vrasic.helpmehelpyou.presentation.play;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vrasic.helpmehelpyou.R;

import org.greenrobot.eventbus.EventBus;

public class GuestPlayFragment extends Fragment {

    public static final String FRAGMENT_TAG = "guestplay-fragment";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        //EventBus.getDefault().register(this);

        View rootView = inflater.inflate(R.layout.guestplay_fragment, container, false);

        return rootView;
    }
}
