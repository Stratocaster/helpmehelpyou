package com.vrasic.helpmehelpyou.presentation.onboarding;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.vrasic.helpmehelpyou.R;
import com.vrasic.helpmehelpyou.data.ProfileRepository;
import com.vrasic.helpmehelpyou.model.Profile;
import com.vrasic.helpmehelpyou.service.AuthManager;

public class SignInFragment extends Fragment {

    public static final String FRAGMENT_TAG = "sign-in-fragment";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sign_in_fragment, container, false);

        final TextInputEditText editText = view.findViewById(R.id.username_edit_text);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    SignInFragment.this.saveUsername(v.getText().toString());
                }
                return handled;
            }
        });

        Button saveButton = view.findViewById(R.id.username_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignInFragment.this.saveUsername(editText.getText().toString());
                InputMethodManager imm = (InputMethodManager) SignInFragment.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            }
        });


        return view;
    }

    private void saveUsername(String username) {
        if (username == null || username.isEmpty()) {
            Toast.makeText(this.getContext(),"Please enter valid username",Toast.LENGTH_SHORT).show();
        }
        else {
            // Ugly workaround to avoid checking username uniqueness:
            // Take last 4 characters of userId and append them to username
            String userId = AuthManager.getInstance().getCurrentUserUid();
            String suffix = userId.substring(userId.length() - 4);
            ProfileRepository.getInstance().saveProfile(new Profile(username + "_" + suffix, null));
        }
    }


}
