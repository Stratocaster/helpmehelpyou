package com.vrasic.helpmehelpyou.data.network;

import com.vrasic.helpmehelpyou.data.network.model.TokenRequest;
import com.vrasic.helpmehelpyou.data.network.model.TokenResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface TokenService {

    @POST("/webrtc/1/token")
    @Headers("Content-Type: application/json")
    Call<TokenResponse> getAccessToken(@Body TokenRequest tokenRequest, @Header("Authorization") String authorization);

}
