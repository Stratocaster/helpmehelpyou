package com.vrasic.helpmehelpyou.data.network.model;

public class TokenRequest {

    public String identity;
    public String displayName;

    public TokenRequest() {}

    public TokenRequest(String identity, String displayName) {
        this.identity = identity;
        this.displayName = displayName;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "TokenRequest{" +
                "identity='" + identity + '\'' +
                ", displayName='" + displayName + '\'' +
                '}';
    }
}
