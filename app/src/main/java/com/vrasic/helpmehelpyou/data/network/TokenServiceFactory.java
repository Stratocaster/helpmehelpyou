package com.vrasic.helpmehelpyou.data.network;

import com.vrasic.helpmehelpyou.data.network.TokenService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TokenServiceFactory {

    public static final String TOKEN_API_BASE_URL = "https://api.infobip.com";
    public static final String CREDENTIALS = "V2ViUlRDVGVzdDpARz9rLnAmelt1OFd6cTZa";

    public static TokenService create() {
        return new Retrofit.Builder()
                .baseUrl(TOKEN_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(TokenService.class);
    }
}
