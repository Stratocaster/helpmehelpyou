package com.vrasic.helpmehelpyou.data.network.model;

import java.util.Date;

public class TokenResponse {
    private String token;
    private String identity;
    private Date expirationTime;

    public TokenResponse() {}

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }


    @Override
    public String toString() {
        return "TokenResponse{" +
                "token='" + token + '\'' +
                ", identity='" + identity + '\'' +
                ", expirationTime=" + expirationTime +
                '}';
    }
}
