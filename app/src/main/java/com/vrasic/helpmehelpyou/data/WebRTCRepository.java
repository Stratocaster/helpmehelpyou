package com.vrasic.helpmehelpyou.data;

import androidx.annotation.MainThread;
import androidx.core.util.Consumer;

import com.vrasic.helpmehelpyou.data.network.model.TokenResponse;
import com.vrasic.helpmehelpyou.data.network.model.TokenRequest;
import com.vrasic.helpmehelpyou.data.network.TokenServiceFactory;
import com.vrasic.helpmehelpyou.model.WebRTCAccessToken;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class WebRTCRepository {

    private static WebRTCRepository instance;
    private WebRTCAccessToken webRTCAccessToken;

    @MainThread
    public static WebRTCRepository getInstance() {
        if (instance == null) {
            instance = new WebRTCRepository();
        }
        return instance;
    }

    public void getAccessToken(String identity, Consumer<WebRTCAccessToken> callback) {
        if (callback == null) {
            return;
        }

        if (this.hasValidAccessToken(identity)) {
            callback.accept(this.webRTCAccessToken);
            return;
        }

        this.loadAccessToken(identity, callback);
    }

    private boolean hasValidAccessToken(String identity) {
        return this.webRTCAccessToken != null;
//        return (webRTCAccessToken != null && webRTCAccessToken.identity != null && webRTCAccessToken.expirationTime != null &&
//            webRTCAccessToken.identity.equals(identity) &&
//            new Date(webRTCAccessToken.expirationTime).after(new Date()));
    }

    private void loadAccessToken(String identity, Consumer<WebRTCAccessToken> callback) {
        TokenRequest tokenRequest = new TokenRequest();
        tokenRequest.setIdentity(identity);
        tokenRequest.setDisplayName(identity);
        String authHeader = "Basic " + TokenServiceFactory.CREDENTIALS;
        Call<TokenResponse> accessTokenCall = TokenServiceFactory.create().getAccessToken(tokenRequest, authHeader);

        Timber.i("Fetching new WebRTC access token for identity %s ...", identity);

        accessTokenCall.enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                WebRTCAccessToken webRTCAccessToken = null;

                if (!response.isSuccessful()) {
                    Timber.w("Problem fetching WebRTC access token! Response code: %s", response.code());
                }
                else if (response.body() == null || response.body().getToken() == null || response.body().getExpirationTime() == null) {
                    Timber.w("Fetched empty WebRTC access token!");
                }
                else {
                    Timber.i("Successfully fetched WebRTC access token %s", response.body());
                    webRTCAccessToken = new WebRTCAccessToken(response.body().getToken(), response.body().getIdentity(), response.body().getExpirationTime());
                }


                WebRTCRepository.getInstance().webRTCAccessToken = webRTCAccessToken;

                if (callback != null) {
                    callback.accept(webRTCAccessToken);
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Timber.e(t, "Error getting WebRTC access token");
                if (callback != null) {
                    callback.accept(null);
                }
            }
        });
    }



}
