package com.vrasic.helpmehelpyou.data;

import androidx.annotation.MainThread;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrasic.helpmehelpyou.event.GameLoaded;
import com.vrasic.helpmehelpyou.model.Game;
import com.vrasic.helpmehelpyou.model.Player;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import timber.log.Timber;


public class GameRepository {

    private static GameRepository instance;
    private DatabaseReference db;
    private ValueEventListener gameListener;
    private String gameId;
    private Game game;
    private boolean intialLoadingFinished = false;


    @MainThread
    public static GameRepository getInstance() {
        if (instance == null) {
            instance = new GameRepository();
            instance.db = FirebaseDatabase.getInstance().getReference();
        }
        return instance;
    }

    public Game getGame() {
        if (this.game != null) {
            return this.game.makeClone();
        }

        return null;
    }

    public boolean hasFinishedInitialLoading() {
        return this.intialLoadingFinished;
    }

    public void startListeningForGameChanges(String gameId) {
        if (this.gameId != null && !this.gameId.equals(gameId) && this.gameListener != null) {
            this.stopListeningForGameChanges();
        }

        this.gameId = gameId;

        if (null == this.gameId) {
            Timber.i("Not proceeding because gameId is not available;");
            return;
        }

        if (null != this.gameListener) {
            Timber.i("Already listening for game changes!");
            return;
        }

        ValueEventListener gamesListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GameRepository.this.intialLoadingFinished = true;

                Game game = dataSnapshot.getValue(Game.class);
                if (game != null) {
                    game.id = dataSnapshot.getKey();

                    if (game.players != null) {
                        HashMap<String, Player> newPlayers = new HashMap<>();

                        Iterator it = game.players.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry)it.next();
                            String key = (String) pair.getKey();
                            Player player = (Player) pair.getValue();
                            player.id = key;
                            newPlayers.put(key, player);
                        }

                        game.players = newPlayers;
                    }
                }

                GameRepository.this.game = game;

                EventBus.getDefault().post(new GameLoaded(GameRepository.this.getGame()));
            }

            @Override
            public void onCancelled(DatabaseError e) {
                Timber.e(e.toException(), "Error while listening to game changes!");
                GameRepository.this.game = null;
                EventBus.getDefault().post(new GameLoaded(GameRepository.this.getGame()));
            }
        };

        this.gameListener = this.db.child("games").child(gameId).addValueEventListener(gamesListener);
    }

    public void stopListeningForGameChanges() {
        if (null == this.gameId) {
            Timber.i("Not proceeding because gameId is not available;");
            return;
        }

        if (null == this.gameListener) {
            Timber.i("No active listeners for game changes!");
            return;
        }

        this.db.child("games").child(this.gameId).removeEventListener(this.gameListener);

        this.gameListener = null;
    }
}
