package com.vrasic.helpmehelpyou.data;


import androidx.annotation.MainThread;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vrasic.helpmehelpyou.event.ProfileLoaded;
import com.vrasic.helpmehelpyou.model.Profile;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import timber.log.Timber;

public class ProfileRepository {
    private static ProfileRepository instance;
    private DatabaseReference db;
    private String userId;
    private ValueEventListener profileListener;
    private Profile profile;
    private boolean intialLoadingFinished = false;

    @MainThread
    public static ProfileRepository getInstance() {
        if (instance == null) {
            instance = new ProfileRepository();
            instance.db = FirebaseDatabase.getInstance().getReference();
        }
        return instance;
    }

    public void saveProfile(Profile profile) {
        if (null == this.userId) {
            Timber.i("Not proceeding because userId is not available;");
            return;
        }

        HashMap<String, Object> updates = new HashMap<>();

        updates.put("profiles/" + this.userId, profile.toMap());

        this.db.updateChildren(updates);
    }

    public Profile getProfile()  {
        if (this.profile != null) {
            return this.profile.makeClone();
        }

        return null;
    }

    public boolean hasFinishedInitialLoading() {
        return this.intialLoadingFinished;
    }

    public void startListeningForProfileChanges(String userId) {
        if (this.userId != null && !this.userId.equals(userId) && this.profileListener != null) {
            this.stopListeningForProfileChanges();
        }

        this.userId = userId;

        if (null == this.userId) {
            Timber.i("Not proceeding because userId is not available;");
            return;
        }

        if (null != this.profileListener) {
            Timber.i("Already listening for profile changes for userId: %s", userId);
            return;
        }

        ValueEventListener profileListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ProfileRepository.this.intialLoadingFinished = true;
                Profile profile = dataSnapshot.getValue(Profile.class);
                if (profile != null) {
                    profile.id = dataSnapshot.getKey();
                }
                ProfileRepository.this.profile = profile;
                EventBus.getDefault().post(new ProfileLoaded(ProfileRepository.this.getProfile()));
            }

            @Override
            public void onCancelled(DatabaseError e) {
                Timber.e(e.toException(), "Error while listening to profile changes!");
                ProfileRepository.this.profile = null;
                EventBus.getDefault().post(new ProfileLoaded(null));
            }
        };

        this.profileListener = this.db.child("profiles").child(this.userId).addValueEventListener(profileListener);
    }

    public void stopListeningForProfileChanges() {
        if (null == this.userId) {
            Timber.i("Not proceeding because userId is not available;");
            return;
        }

        if (null == this.profileListener) {
            Timber.i("No active listeners for profile changes!");
            return;
        }

        this.db.child("profiles").child(this.userId).removeEventListener(this.profileListener);
        this.profileListener = null;
    }

}
