package com.vrasic.helpmehelpyou.data;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.vrasic.helpmehelpyou.event.GamesLoaded;
import com.vrasic.helpmehelpyou.model.Game;
import com.vrasic.helpmehelpyou.model.Player;
import com.vrasic.helpmehelpyou.model.Profile;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import timber.log.Timber;

public class GamesRepository {

    private static GamesRepository instance;
    private DatabaseReference db;
    private ValueEventListener gamesListener;
    private CopyOnWriteArrayList<Game> games = new CopyOnWriteArrayList<>();
    private boolean intialLoadingFinished = false;

    @MainThread
    public static GamesRepository getInstance() {
        if (instance == null) {
            instance = new GamesRepository();
            instance.db = FirebaseDatabase.getInstance().getReference();
        }
        return instance;
    }

    public CopyOnWriteArrayList<Game> getGames() {
        return new CopyOnWriteArrayList<>(this.games);
    }

    public boolean hasFinishedInitialLoading() {
        return this.intialLoadingFinished;
    }

    public void startListeningForGamesChanges() {
        if (null != this.gamesListener) {
            Timber.i("Already listening for games changes!");
            return;
        }

        ValueEventListener gamesListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GamesRepository.this.intialLoadingFinished = true;

                GamesRepository.this.games.clear();
                for(DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    Game game = childDataSnapshot.getValue(Game.class);
                    game.id = childDataSnapshot.getKey();
                    GamesRepository.this.games.add(game);
                }

                EventBus.getDefault().post(new GamesLoaded(GamesRepository.this.getGames()));
            }

            @Override
            public void onCancelled(DatabaseError e) {
                Timber.e(e.toException(), "Error while listening to games changes!");
                GamesRepository.this.games.clear();
                EventBus.getDefault().post(new GamesLoaded(new CopyOnWriteArrayList<>(GamesRepository.this.games)));
            }
        };

        this.gamesListener = this.db.child("games").addValueEventListener(gamesListener);
    }

    public void stopListeningForGamesChanges() {
        if (null == this.gamesListener) {
            Timber.i("No active listeners for games changes!");
            return;
        }

        this.db.child("games").removeEventListener(this.gamesListener);

        this.gamesListener = null;
    }

    public void createGame(final Profile profile, final Consumer<String> onFinish) {
        final String gameId = this.db.child("games").push().getKey();

        HashMap<String, Player> players = new HashMap<>();
        players.put(profile.id, new Player(profile.username, true, false));

        Game game = new Game(players, Game.DEFAULT_STATUS, Game.DEFAULT_TIMER, Game.DEFAULT_MISS);

        this.db.child("games").child(gameId).setValue(game.toMap())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Timber.d("Game created successfully: %s", gameId);

                        // Remove this player if he looses connection or app crashes
                        GamesRepository.this.db.child("games").child(gameId).child("players").child(profile.id).onDisconnect().removeValue();

                        if (onFinish != null) {
                            onFinish.accept(gameId);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Timber.d("Error creating game: %s", e.getMessage());
                        if (onFinish != null) {
                            onFinish.accept(null);
                        }
                    }
                });
    }

    public void joinGame(final String gameId, final Profile profile, final Consumer<Boolean> onFinish) {
        this.db.child("games").child(gameId).child("players").runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                HashMap<String, Object> players = new HashMap<>();

                for (MutableData childMutableData: mutableData.getChildren()) {
                    Player player = childMutableData.getValue(Player.class);
                    players.put(childMutableData.getKey(), player.toMap());
                }

                if (players.values().size() > 1) {
                    return Transaction.abort();
                }

                players.put(profile.id, (new Player(profile.username, false, false)).toMap());
                mutableData.setValue(players);

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (!b) {
                    Timber.d("Game not joined!");
                }
                else {
                    Timber.d("Game joined successfully!");

                    // Remove this player if he looses connection or app crashes
                    GamesRepository.this.db.child("games").child(gameId).child("players").child(profile.id).onDisconnect().removeValue();
                }

                if (databaseError != null) {
                    Timber.d("Error while joining game: %s", databaseError.getMessage());
                }

                if (onFinish != null) {
                    onFinish.accept(b);
                }
            }
        });
    }

    public void leaveGame(final String gameId, final String profileId, final Consumer<Boolean> onFinish) {
        this.db.child("games").child(gameId).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Game game = mutableData.getValue(Game.class);

                if (game == null) {
                    return Transaction.abort();
                }

                HashMap<String, Player> players = game.players;

                // Remove entire game
                if (players.values().size() == 0 || (players.values().size() == 1 && players.containsKey(profileId))) {
                    mutableData.setValue(null);
                    return Transaction.success(mutableData);
                }

                // Don't do anything
                if (!players.containsKey(profileId)) {
                    return Transaction.abort();
                }

                // Remove only this player and reset game values
                players.remove(profileId);
                game.players = players;
                game.resetToDefaults();

                mutableData.setValue(game.toMap());

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (!b) {
                    Timber.d("Player did not leave game!");
                }
                else {
                    Timber.d("Player left game successfully!");
                }

                if (databaseError != null) {
                    Timber.d("Error while leaving game: %s", databaseError.getMessage());
                }

                if (onFinish != null) {
                    onFinish.accept(b);
                }
            }
        });
    }

    public void requestJoinApproval(final String gameId, final String profileId, final Consumer<Boolean> onFinish) {
        HashMap<String, Object> updates = new HashMap<>();

        updates.put("lastPlayerRequestingApproval", profileId);
        updates.put("status", Game.Status.WAITING_APPROVAL.getValue());

        this.db.child("games").child(gameId).updateChildren(updates)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Timber.d("Player %s successfully requested approval for game %s", profileId, gameId);
                        if (onFinish != null) {
                            onFinish.accept(true);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Timber.d("Error while player %s requesting approval for game %s: %s", profileId, gameId, e.getMessage());
                        if (onFinish != null) {
                            onFinish.accept(false);
                        }
                    }
                });

    }

    public void approveJoinRequest(final String gameId, final Consumer<Boolean> onFinish) {
        HashMap<String, Object> updates = new HashMap<>();

        updates.put("status", Game.Status.ACTIVE.getValue());

        this.db.child("games").child(gameId).updateChildren(updates)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Timber.d("Join request for game %s successfully approved", gameId);
                        if (onFinish != null) {
                            onFinish.accept(true);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Timber.d("Error while approving join request for game %s: %s", gameId, e.getMessage());
                        if (onFinish != null) {
                            onFinish.accept(false);
                        }
                    }
                });
    }

    public void toggleGameHost(final String gameId, final Consumer<Boolean> onFinish) {
        this.db.child("games").child(gameId).child("players").runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                HashMap<String, Player> players = new HashMap<>();

                for (MutableData childMutableData: mutableData.getChildren()) {
                    Player player = childMutableData.getValue(Player.class);
                    player.id = childMutableData.getKey();
                    players.put(childMutableData.getKey(), player);
                }

                if (players.values().size() != 2) {
                    return Transaction.abort();
                }

                Player hostPlayer = null;
                Player guestPlayer = null;

                for (Player player : players.values()) {
                    if (player.isHost) {
                        hostPlayer = player;
                    }
                    else {
                        guestPlayer = player;
                    }
                }

                hostPlayer.isHost = false;
                guestPlayer.isHost = true;

                HashMap<String, Object> newPlayers = new HashMap<>();

                newPlayers.put(hostPlayer.id, hostPlayer.toMap());
                newPlayers.put(guestPlayer.id, guestPlayer.toMap());

                mutableData.setValue(newPlayers);

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (!b) {
                    Timber.d("Player host not toggled!");
                }
                else {
                    Timber.d("Player host toggled!");
                }

                if (databaseError != null) {
                    Timber.d("Error while toggling players: %s", databaseError.getMessage());
                }

                if (onFinish != null) {
                    onFinish.accept(b);
                }
            }
        });
    }

    public void togglePlayerReady(final String gameId, String profileId, final Consumer<Boolean> onFinish) {
        this.db.child("games").child(gameId).child("players").child(profileId).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                Player player = mutableData.getValue(Player.class);

                if (player == null) {
                    return Transaction.abort();
                }

                player.isReady = !player.isReady;

                mutableData.setValue(player.toMap());

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (!b) {
                    Timber.d("Player ready not toggled!");
                }
                else {
                    Timber.d("Player ready toggled!");
                }

                if (databaseError != null) {
                    Timber.d("Error while toggling player ready state: %s", databaseError.getMessage());
                }

                if (onFinish != null) {
                    onFinish.accept(b);
                }
            }
        });
    }
}
