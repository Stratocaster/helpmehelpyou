package com.vrasic.helpmehelpyou.event;

public class GameCreated {
    private String gameId;

    public GameCreated(String gameId) {
        this.gameId = gameId;
    }

    public String getGameId() {
        return this.gameId;
    }
}
