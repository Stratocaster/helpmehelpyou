package com.vrasic.helpmehelpyou.event;

public class UserSignedOut {

    String userUid;

    public UserSignedOut(String userUid) {
        this.userUid = userUid;
    }

    public String getUserUid() {
        return this.userUid;
    }

}
