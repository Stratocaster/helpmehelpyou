package com.vrasic.helpmehelpyou.event;

public class GameJoined {
    private String gameId;

    public GameJoined(String gameId) {
        this.gameId = gameId;
    }

    public String getGameId() {
        return this.gameId;
    }
}
