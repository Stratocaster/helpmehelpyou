package com.vrasic.helpmehelpyou.event;

import com.infobip.webrtc.sdk.api.event.CallEventListener;
import com.infobip.webrtc.sdk.api.event.call.CallEarlyMediaEvent;
import com.infobip.webrtc.sdk.api.event.call.CallErrorEvent;
import com.infobip.webrtc.sdk.api.event.call.CallEstablishedEvent;
import com.infobip.webrtc.sdk.api.event.call.CallHangupEvent;
import com.infobip.webrtc.sdk.api.event.call.CallRingingEvent;

import timber.log.Timber;

public class CallListener implements CallEventListener {
    @Override
    public void onEarlyMedia(CallEarlyMediaEvent callEarlyMediaEvent) {
        Timber.i("EarlyMedia");
    }

    @Override
    public void onEstablished(CallEstablishedEvent callEstablishedEvent) {
        Timber.i("CallEstablished: %s", callEstablishedEvent.toString());
    }

    @Override
    public void onHangup(CallHangupEvent callHangupEvent) {
        Timber.i("Hangup");
    }

    @Override
    public void onError(CallErrorEvent callErrorEvent) {
        Timber.e("Error calling %s", callErrorEvent.getReason().getDescription());

    }

    @Override
    public void onRinging(CallRingingEvent callRingingEvent) {
        Timber.i("Calling other side");
    }
}
