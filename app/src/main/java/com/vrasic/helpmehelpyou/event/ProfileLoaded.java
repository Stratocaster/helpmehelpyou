package com.vrasic.helpmehelpyou.event;

import com.vrasic.helpmehelpyou.model.Profile;

public class ProfileLoaded {

    private Profile profile;

    public ProfileLoaded(Profile profile) {
        this.profile = profile;
    }

    public Profile getProfile() {
        return this.profile;
    }
}
