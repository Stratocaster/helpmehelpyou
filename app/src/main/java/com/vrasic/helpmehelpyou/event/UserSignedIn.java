package com.vrasic.helpmehelpyou.event;

public class UserSignedIn {

    String userUid;

    public UserSignedIn(String userUid) {
        this.userUid = userUid;
    }

    public String getUserUid() {
        return this.userUid;
    }
}
