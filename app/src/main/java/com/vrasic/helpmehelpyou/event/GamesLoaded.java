package com.vrasic.helpmehelpyou.event;

import com.vrasic.helpmehelpyou.model.Game;

import java.util.concurrent.CopyOnWriteArrayList;

public class GamesLoaded {

    private CopyOnWriteArrayList<Game> games = new CopyOnWriteArrayList<>();

    public GamesLoaded(CopyOnWriteArrayList<Game> games) {
        this.games = games;
    }

    public CopyOnWriteArrayList<Game> getGames() {
        return this.games;
    }
}
