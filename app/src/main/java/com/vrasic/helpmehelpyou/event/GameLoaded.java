package com.vrasic.helpmehelpyou.event;

import com.vrasic.helpmehelpyou.model.Game;

public class GameLoaded {

    private Game game;

    public GameLoaded(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return this.game;
    }
}
