package com.vrasic.helpmehelpyou.service;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.vrasic.helpmehelpyou.event.UserSignedIn;
import com.vrasic.helpmehelpyou.event.UserSignedOut;

import org.greenrobot.eventbus.EventBus;

public class AuthManager implements FirebaseAuth.AuthStateListener {

    private static AuthManager instance;
    private FirebaseAuth auth;

    @MainThread
    public static AuthManager getInstance() {
        if (instance == null) {
            instance = new AuthManager();
        }
        return instance;
    }

    public void init() {
        this.auth = FirebaseAuth.getInstance();
        this.auth.addAuthStateListener(this);
    }

    public void signIn() {
        this.auth.signInAnonymously();

    }

    public void signOut() {
        this.auth.signOut();
    }

    public String getCurrentUserUid() {
        FirebaseUser currentUser = this.auth.getCurrentUser();

        if (null != currentUser) {
            return currentUser.getUid();
        }

        return null;
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        Object event = (null != currentUser)
                ? new UserSignedIn(firebaseAuth.getUid())
                : new UserSignedOut(firebaseAuth.getUid());

        EventBus.getDefault().post(event);
    }
}
