package com.vrasic.helpmehelpyou.service;

import timber.log.Timber;

public class LogManager {

    public static void init() {
        Timber.plant(new Timber.DebugTree());
    }

}
